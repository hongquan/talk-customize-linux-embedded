### BeagleBone

- Original build script: https://github.com/beagleboard/image-builder
- Used for BeagleBone Black and other variants (Green, Blue etc.)
- Can run only on real ARM hardware (not QEMU).
- My customization: https://github.com/AgriConnect/beaglebone-image-builder
