### BeagleBone

My needs

- Add some Debian repositories.
- Remove BoneScript, NodeJS.
- Remove Cloud9 IDE.
- Remove Bone101 documentation (Apache - Jekyll)
- Add libraries needed to build Python extensions later (`libjpeg-dev` to build `Pillow`).
- Install Nginx, Redis, which are needed by my applications
