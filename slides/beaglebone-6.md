### BeagleBone

- _build-image-for-agriconnect.sh_:
	+ Run _./RootStock-NG.sh_ and pass the config file above.
	+ Run *setup_sdcard.sh* to create SD card image file.
	+ Generate _*.bmap_ file, to reduce time writing to card.
	+ Compress as _*.xz_ file.
