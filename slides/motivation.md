## Motivation

- Remove components we don't need (desktop environment)
- Add some softwares we need
- Change the default config of some softwares (disable IPv6 for Avahi, disable DNS proxy of connman)
- Automatically build image on Git push
