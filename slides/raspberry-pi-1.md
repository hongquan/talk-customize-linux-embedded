## Raspberry Pi

- Official OS is Raspbian, based on Debian.
- Original build script: https://github.com/RPi-Distro/pi-gen
- Original build script can run under QEMU + Docker.
- My customization: https://github.com/fossasia/pi-gen, cannot run under QEMU.
