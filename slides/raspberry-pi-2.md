### Raspberry Pi

- Build script ([`pi-gen`](https://github.com/RPi-Distro/pi-gen)) is better organized than BeagleBone.
- The process is split to multiple stages, where more and more softwares are added.
- After finishing a stage, all files will be copied to next stage, to be the base for next stage.
- Stage 2 is to produce console OS (no X server, no GUI).
- Stage 3+ is to produce desktop OS.
