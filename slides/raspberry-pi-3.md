### Raspberry Pi

My needs:

- Include our [repository](https://gemfury.com/fossasia) for pip, with pre-built wheel files for Python packages (e.g. [snowboy](https://github.com/Kitt-AI/snowboy/) etc.).
- Include the libraries and 3rd-party softwares needed by [susi_linux](https://github.com/fossasia/susi_linux/).
- Install susi_linux (and run its _install.sh_ script to install drivers).
