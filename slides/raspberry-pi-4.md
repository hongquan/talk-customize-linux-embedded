### Raspberry Pi

- Our Raspbian doesn't need GUI. It is not a desktop OS → Cannot customize build script at stage 3+.
- We avoid modifying 2, to avoid rebuilding it every time we make a change.

![pi-gen stages](img/rpi-stages.png)
