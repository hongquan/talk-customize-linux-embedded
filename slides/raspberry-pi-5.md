### Raspberry Pi

- Fortunately, [`pi-gen`](https://github.com/RPi-Distro/pi-gen) recognizes "stage2.5". Our customization will be here.

    ```sh
    $ exa --tree stage2.5
    stage2.5
    ├── 01-susi
    │  ├── 00-packages           # List packages to be installed by APT
    │  ├── 01-run-chroot.sh
    │  └── 02-run.sh
    ├── EXPORT_IMAGE             # Empty file
    └── prerun.sh                # Same as other stages
    ```
