### Raspberry Pi

Experience:

- Inside chroot, don't restart `systemd-udevd`, or the build script will fail.
- If your application needs Java, remove Java with `apt purge` then install again.
