### Raspberry Pi

Further customization:
- Compress image with `xz`, instead of `zip`.
- Naming file with our scheme: `SUSIbian_[branch]_[date]_[PR]_[revision]`

This, unfortunately, requires to modify files outside our stage.
